{"audit_control", CAP_AUDIT_CONTROL},

{"audit_read", CAP_AUDIT_READ},

{"audit_write", CAP_AUDIT_WRITE},

{"block_suspend", CAP_BLOCK_SUSPEND},

{"bpf", CAP_BPF},

{"checkpoint_restore", CAP_CHECKPOINT_RESTORE},

{"chown", CAP_CHOWN},

{"dac_override", CAP_DAC_OVERRIDE},

{"dac_read_search", CAP_DAC_READ_SEARCH},

{"fowner", CAP_FOWNER},

{"fsetid", CAP_FSETID},

{"ipc_lock", CAP_IPC_LOCK},

{"ipc_owner", CAP_IPC_OWNER},

{"kill", CAP_KILL},

{"lease", CAP_LEASE},

{"linux_immutable", CAP_LINUX_IMMUTABLE},

{"mac_admin", CAP_MAC_ADMIN},

{"mac_override", CAP_MAC_OVERRIDE},

{"mknod", CAP_MKNOD},

{"net_admin", CAP_NET_ADMIN},

{"net_bind_service", CAP_NET_BIND_SERVICE},

{"net_broadcast", CAP_NET_BROADCAST},

{"net_raw", CAP_NET_RAW},

{"perfmon", CAP_PERFMON},

{"setfcap", CAP_SETFCAP},

{"setgid", CAP_SETGID},

{"setpcap", CAP_SETPCAP},

{"setuid", CAP_SETUID},

{"syslog", CAP_SYSLOG},

{"sys_admin", CAP_SYS_ADMIN},

{"sys_boot", CAP_SYS_BOOT},

{"sys_chroot", CAP_SYS_CHROOT},

{"sys_module", CAP_SYS_MODULE},

{"sys_nice", CAP_SYS_NICE},

{"sys_pacct", CAP_SYS_PACCT},

{"sys_ptrace", CAP_SYS_PTRACE},

{"sys_rawio", CAP_SYS_RAWIO},

{"sys_resource", CAP_SYS_RESOURCE},

{"sys_time", CAP_SYS_TIME},

{"sys_tty_config", CAP_SYS_TTY_CONFIG},

{"wake_alarm", CAP_WAKE_ALARM},

